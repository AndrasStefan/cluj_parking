from django.db import models
from djgeojson.fields import PointField


class Parking(models.Model):
    description = models.CharField(max_length=100)
    free = models.BooleanField(default=True)
    geom = PointField()

    @property
    def get_button_name(self):
        return 'Occupy' if self.free else 'Free'

    def __str__(self):
        return '{} - {} - {}'.format(self.description, self.free, self.geom)

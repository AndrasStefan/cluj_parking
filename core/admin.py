from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin

from core.models import Parking


class LargerGeoAdmin(LeafletGeoAdmin):
    map_height = '600px'


admin.site.register(Parking, LargerGeoAdmin)

from django.urls import path

from core import views

urlpatterns = [
    path('', views.index, name='index'),
    path('edit/<int:pk>', views.edit, name='edit'),
]

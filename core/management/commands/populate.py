import random

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import transaction

from core.models import Parking


class Command(BaseCommand):
    help = "Restart the state of database."

    @staticmethod
    def _create_admin():
        User.objects.create_superuser(username="admin", password="admin", email="admin@yahoo.com")

    @transaction.atomic
    def _create_parkings(self):
        first_point = (23.550395965576172, 46.78619146006186)
        second_point = (23.643951416015625, 46.752093935938696)

        for i in range(10000):
            park = Parking.objects.create(
                description='parking{}'.format(i),
                free=random.choice([True, False]),
                geom={'type': 'Point', 'coordinates': [
                    random.uniform(first_point[0], second_point[0]),
                    random.uniform(first_point[1], second_point[1])
                ]},
            )
            self.stdout.write(self.style.SUCCESS(park))

    def handle(self, *args, **options):
        self._create_admin()
        self._create_parkings()
        self.stdout.write(self.style.SUCCESS('Done.'))

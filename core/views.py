from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt

from core.models import Parking


def index(request):
    return render(request, 'index.html', {
        'parkings': Parking.objects.all(),
        'free_count': Parking.objects.filter(free=True).count(),
        'occupied_count': Parking.objects.filter(free=False).count(),
    })


@csrf_exempt
def edit(request, pk):
    parking = get_object_or_404(Parking, pk=pk)
    parking.free = not parking.free
    parking.save()
    return redirect('index')

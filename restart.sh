#!/usr/bin/env bash

rm db.sqlite3
rm -rf core/migrations
python manage.py makemigrations
python manage.py makemigrations core
python manage.py migrate
python manage.py populate
